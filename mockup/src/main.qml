import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.0
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 490
    height: 610
    maximumWidth: root.width
    maximumHeight: root.height
    minimumWidth: root.width
    minimumHeight: root.height
    color: "transparent"
    flags: Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint | Qt.Window

    property int animDuration: 10

    MouseArea{
        anchors.fill: parent
        property var clickPos: "1,1"

        onPressed: {
            clickPos = Qt.point( mouse.x,  mouse.y)
        }

        onPositionChanged: {
            var delta = Qt.point(mouse.x - clickPos.x, mouse.y - clickPos.y);
            root.x += delta.x;
            root.y += delta.y;
        }

    }



    Rectangle{
        id: background_rec
        visible: true
        width: root.width
        height: root.height
        color: colors.dark_background
        radius: 40

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.Top
            y: 10
            text: "Calculator 2018"
            color: button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

        Behavior on color
        {
            ColorAnimation{ duration: root.animDuration}
        }

    }


    Rectangle{

        id: cancelButton
        x: root.width - (cancelButton.width +15)
        y: 15
        width: 20
        height: 20
        color: mouse_cancel.containsMouse ? mouse_cancel.containsPress ? "red" :  "#ff4d4d" : operation_button.color
        radius: 100


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }

        MouseArea{
            id: mouse_cancel
            anchors.fill: parent
            hoverEnabled: true
            onClicked: root.close()

        }
    }

    Rectangle{

        id: hideButton
        x: root.width - (cancelButton.width +15 + hideButton.width + 5)
        y: 15
        width: 20
        height: 20
        color: mouse_hide.containsMouse ? mouse_hide.containsPress ? "#0073e6" :  "#80bfff" : operation_button.color
        radius: 100


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }

        MouseArea{
            id: mouse_hide
            anchors.fill: parent
            hoverEnabled: true
            onClicked: root.showMinimized()

        }
    }


    Rectangle{

        id: screen
        x:0
        y:50
        width: root.width
        height: 150
        color: colors.dark_screen
        opacity: 1

        Behavior on color
        {
            ColorAnimation{ duration: root.animDuration}
        }

        Text{
            id: screenText
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 10

            text: "0"
            color: "white"
            font.pointSize: 52
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }
        }
    }

    Rectangle{

        id: correctBar
        x:0
        y:screen.y+ screen.height + 20
        width: root.width
        height: 10
        color: "#36bf66"


        Behavior on color {
            ColorAnimation {from: "red"; to: "#36bf66"; duration: 1000;}
        }

    }


    Rectangle{
        id: colors
        property color dark_numbers_button: "#262626"
        property color dark_numbers_button_light: "#4f597d"
        property color dark_numbers_button_click: "#6776ad"
        property color dark_operation_button: "#121212"
        property color dark_operation_button_light: "#444040"
        property color dark_operation_button_click: "#646262"
        property color dark_button_text: "#817e7e"
        property color dark_button_text_clicked: "white"
        property color dark_screen: "#5e5e5e"
        property color dark_background: "#0b0a0a"


        property color white_screen: "#f0f0f0"
        property color white_numbers_button: "#bbb9b9"
        property color white_numbers_button_light: "#4d75cb"
        property color white_numbers_button_click: "#819dda"
        property color white_operation_button: "#97989b"
        property color white_operation_button_light: "#aeafb2"
        property color white_operation_button_click: "#cbcccd"
        property color white_button_text: "#1a1a1a"
        property color white_button_text_clicked: "#000000"
        property color white_background: "#d4d3d3"

    }

    Rectangle{
        id: numbers_button
        color: colors.dark_numbers_button

    }

    Rectangle{
        id: numbers_button_light
        color: colors.dark_numbers_button_light
    }

    Rectangle{
        id: numbers_button_click
        color: colors.dark_numbers_button_click
    }

    Rectangle{
        id: operation_button
        color: colors.dark_operation_button
    }

    Rectangle{
        id: operation_button_light
        color: colors.dark_operation_button_light
    }

    Rectangle{
        id: operation_button_click
        color: colors.dark_operation_button_click
    }

    Text{

        id:button_text
        font.pointSize: 12
        font.family: "Abel"
        color: colors.dark_button_text
    }

    Text{

        id:button_text_onClicked
        color:colors.dark_button_text_clicked

    }



    Rectangle{
       id: button_template
        x: 2
        y: correctBar.y + correctBar.height + (correctBar.y - (screen.y + screen.height))
        width: 120
        height: 50
        radius: 0
        color:  mouse_template.containsMouse ? mouse_template.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }



        MouseArea{

            id: mouse_template
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "%"
            color: mouse_template.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }


    }


    Rectangle{
       id: button1_1
        x: 2*button_template.x + button_template.width
        y: button_template.y
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template1.containsMouse ? mouse_template1.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template1
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "√"
            color: mouse_template1.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button2_1
        x: button1_1.x + button1_1.width + button_template.x
        y: button1_1.y
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template2.containsMouse ? mouse_template2.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template2
            anchors.fill: parent
            hoverEnabled: true

        }


        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "x^2"
            color: mouse_template2.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button3_1
        x: button2_1.x + button2_1.width + button_template.x
        y: button1_1.y
        width: button_template.width
        height: button_template.height
        radius: button_template.radius

        color:  mouse_template3.containsMouse ? mouse_template3.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation { duration:  root.animDuration }
                }


        MouseArea{

            id: mouse_template3
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "1/x"
            color: mouse_template3.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    //Druhy rad

    Rectangle{
       id: button1_2
        x: button_template.x
        y: button_template.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template4.containsMouse ? mouse_template4.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template4
            anchors.fill: parent
            hoverEnabled: true

        }
        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "CE"
            color: mouse_template4.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button2_2
        x: 2*button_template.x + button_template.width
        y: button_template.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template5.containsMouse ? mouse_template5.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template5
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "C"
            color: mouse_template5.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button3_2
        x: button1_1.x + button1_1.width + button_template.x
        y: button_template.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template6.containsMouse ? mouse_template6.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template6
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "←"
            color: mouse_template6.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button4_2
        x: button2_1.x + button2_1.width + button_template.x
        y: button_template.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template7.containsMouse ? mouse_template7.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template7
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "÷"
            color: mouse_template7.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    //Treti rad

    Rectangle{
       id: button1_3
        x: button_template.x
        y: button1_2.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template8.containsMouse ? mouse_template8.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template8
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "7"
            color: mouse_template8.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family


            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button2_3
        x: 2*button_template.x + button_template.width
        y: button1_2.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template9.containsMouse ? mouse_template9.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template9
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "8"
            color: mouse_template9.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button3_3
        x: button1_1.x + button1_1.width + button_template.x
        y: button1_2.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template10.containsMouse ? mouse_template10.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template10
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "9"
            color: mouse_template10.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button4_3
        x: button2_1.x + button2_1.width + button_template.x
        y: button1_2.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template11.containsMouse ? mouse_template11.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template11
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "X"
            color: mouse_template11.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    //Stvrty rad

    Rectangle{
       id: button1_4
        x: button_template.x
        y: button1_3.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template12.containsMouse ?  mouse_template12.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template12
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "4"
            color: mouse_template12.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button2_4
        x: 2*button_template.x + button_template.width
        y: button1_3.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template13.containsMouse ?  mouse_template13.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template13
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "5"
            color: mouse_template13.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button3_4
        x: button1_1.x + button1_1.width + button_template.x
        y: button1_3.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template14.containsMouse ?  mouse_template14.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template14
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "6"
            color: mouse_template14.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button4_4
        x: button2_1.x + button2_1.width + button_template.x
        y: button1_3.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template15.containsMouse ? mouse_template15.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template15
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "-"
            color: mouse_template15.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    // Piaty Rad

    Rectangle{
       id: button1_5
        x: button_template.x
        y: button1_4.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template16.containsMouse ? mouse_template16.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template16
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "1"
            color: mouse_template16.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button2_5
        x: 2*button_template.x + button_template.width
        y: button1_4.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template17.containsMouse ? mouse_template17.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template17
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "2"
            color: mouse_template17.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button3_5
        x: button1_1.x + button1_1.width + button_template.x
        y: button1_4.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template18.containsMouse ? mouse_template18.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template18
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "3"
            color: mouse_template18.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button4_5
        x: button2_1.x + button2_1.width + button_template.x
        y: button1_4.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template19.containsMouse ? mouse_template19.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template19
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "+"
            color: mouse_template19.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    //Siesty rad

    Rectangle{
       id: button1_6
        x: button_template.x
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template20.containsMouse ? mouse_template20.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template20
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "±"
            color: mouse_template20.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button2_6
        x: 2*button_template.x + button_template.width
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template21.containsMouse ? mouse_template21.containsPress ? numbers_button_click.color : numbers_button_light.color : numbers_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template21
            anchors.fill: parent
            hoverEnabled: true

        }


        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "0"
            color: mouse_template21.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button3_6
        x: button1_1.x + button1_1.width + button_template.x
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template22.containsMouse ?  mouse_template22.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }


        MouseArea{

            id: mouse_template22
            anchors.fill: parent
            hoverEnabled: true
        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: ","
            color: mouse_template22.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: button4_6
        x: button2_1.x + button2_1.width + button_template.x
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template23.containsMouse ?  mouse_template23.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template23
            anchors.fill: parent
            hoverEnabled: true
            onClicked: { correctBar.color = "red";}

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "="
            color: mouse_template23.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }
        }

    }


    Rectangle{
        id: theme
        x: 15
        y: root.height - (theme.width + 15)
        width: 20
        height: 20
        radius: 100
        color: colors.dark_operation_button


        property string dark_theme: "dark"
        property string white_theme: "white"
        property string theme_name: theme.dark_theme

        Behavior on color{
            ColorAnimation { duration: root.animDuration}
        }


        MouseArea{

            id: mouse_theme
            anchors.fill: parent
            onClicked: theme._toggleTheme()

        }

        function applyDark()
        {
            theme.theme_name = theme.dark_theme;
            theme.color = colors.dark_operation_button;
            screen.color = colors.dark_screen;
            numbers_button.color = colors.dark_numbers_button;
            numbers_button_click.color = colors.dark_numbers_button_click;
            numbers_button_light.color = colors.dark_numbers_button_light;
            operation_button.color = colors.dark_operation_button;
            operation_button_click.color = colors.dark_operation_button_click;
            operation_button_light.color = colors.dark_operation_button_light;
            background_rec.color = colors.dark_background;
            button_text.color = colors.dark_button_text;
            screenText.color = "white"
        }

        function applyLight()
        {
            theme.theme_name = theme.white_theme;
            theme.color = "#36bf66";
            screen.color = colors.white_screen;
            numbers_button.color = colors.white_numbers_button;
            numbers_button_click.color = colors.white_numbers_button_click;
            numbers_button_light.color = colors.white_numbers_button_light;
            operation_button.color = colors.white_operation_button;
            operation_button_click.color = colors.white_operation_button_click;
            operation_button_light.color = colors.white_operation_button_light;
            background_rec.color = colors.white_background;
            button_text.color = colors.white_button_text;
            screenText.color = "black"
        }

        function _toggleTheme(){

            if (theme.theme_name == "white")
                applyDark()
            else
                applyLight()
        }

    }


    Rectangle{

        id: scienceCalcButton
        x: root.width - (cancelButton.width +15)
        y: root.height - (theme.width + 15)
        width: 20
        height: 20
        color:  operation_button.color
        radius: 100
        property string isScaled: "CLASSIC"


        Behavior on color {
                    ColorAnimation {duration: root.animDuration}
                }

        MouseArea{

            id: mouse_science_calc
            anchors.fill: parent
            hoverEnabled: true
            onClicked: scienceCalcButton.scaleWindow()

        }


        function scaleWindow()
        {

            switch (isScaled)
            {
            case "CLASSIC":
                scienceCalcButton.color = "#ff8c1a"
                root.width = buttonScience_1_2.x + buttonScience_1_2.width + button_template.x
                background_rec.width = buttonScience_1_2.x + buttonScience_1_2.width + button_template.x
                correctBar.width = buttonScience_1_2.x + buttonScience_1_2.width + button_template.x
                screen.width = buttonScience_1_2.x + buttonScience_1_2.width + button_template.x
                isScaled = "SCIENCE";
                break;
            case "SCIENCE":
                root.width = graphScreen.x + graphScreen.width + button_template.x + 10
                background_rec.width = graphScreen.x + graphScreen.width + button_template.x + 10
                correctBar.width = buttonScience_1_2.x + buttonScience_1_2.width + button_template.x
                screen.width =buttonScience_1_2.x + buttonScience_1_2.width + button_template.x
                isScaled = "GRAPH";
                break;
            case "GRAPH":
                scienceCalcButton.color = operation_button.color
                root.width = 490
                background_rec.width = 490
                correctBar.width = 490
                screen.width = 490
                isScaled = "CLASSIC";
                break;


            }

        }

    }


    //Vedecka kalkulacka

    Rectangle{
       id: buttonScience_1_1
        x: button3_1.x + button3_1.width + button_template.x
        y: button1_1.y
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template24.containsMouse ? mouse_template24.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation { duration:  root.animDuration }
                }


        MouseArea{

            id: mouse_template24
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "x^y"
            color: mouse_template24.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_1_2
        x: buttonScience_1_1.x + buttonScience_1_1.width + button_template.x
        y: button1_1.y
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template25.containsMouse ? mouse_template25.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color


        Behavior on color {
                    ColorAnimation { duration:  root.animDuration }
                }


        MouseArea{

            id: mouse_template25
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "sin"
            color: mouse_template25.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_2_1
        x: button3_1.x + button3_1.width + button_template.x
        y: button_template.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template26.containsMouse ?  mouse_template26.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template26
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "cos"
            color: mouse_template26.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_2_2
        x: buttonScience_1_1.x + buttonScience_1_1.width + button_template.x
        y: button_template.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template27.containsMouse ?  mouse_template27.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template27
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "tan"
            color: mouse_template27.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_3_1
        x: button3_1.x + button3_1.width + button_template.x
        y: button1_2.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template28.containsMouse ?  mouse_template28.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template28
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "10^x"
            color: mouse_template28.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_3_2
        x: buttonScience_1_1.x + buttonScience_1_1.width + button_template.x
        y: button1_2.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template29.containsMouse ?  mouse_template29.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template29
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "log"
            color: mouse_template29.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_4_1
        x: button3_1.x + button3_1.width + button_template.x
        y: button1_3.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template30.containsMouse ?  mouse_template30.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template30
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "Exp"
            color: mouse_template30.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_4_2
        x: buttonScience_1_1.x + buttonScience_1_1.width + button_template.x
        y: button1_3.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template31.containsMouse ?  mouse_template31.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template31
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "Mod"
            color: mouse_template31.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }
    }

    Rectangle{
       id: buttonScience_5_1
        x: button3_1.x + button3_1.width + button_template.x
        y: button1_4.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template32.containsMouse ?  mouse_template32.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template32
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "π"
            color: mouse_template32.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_5_2
        x: buttonScience_1_1.x + buttonScience_1_1.width + button_template.x
        y: button1_4.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template33.containsMouse ?  mouse_template33.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template33
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "n!"
            color: mouse_template33.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_6_1
        x: button3_1.x + button3_1.width + button_template.x
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template34.containsMouse ?  mouse_template34.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template34
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "("
            color: mouse_template34.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonScience_6_2
        x: buttonScience_1_1.x + buttonScience_1_1.width + button_template.x
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.width
        height: button_template.height
        radius: button_template.radius
        color:  mouse_template35.containsMouse ?  mouse_template35.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }

        MouseArea{

            id: mouse_template35
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: ")"
            color: mouse_template35.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
        id:graphScreen
        x: screen.width + button_template.x + 10
        y: screen.y
        width: (button1_4.y + button1_4.height) - graphScreen.y
        height: (button1_4.y + button1_4.height) - graphScreen.y
        color: screen.color

    }


    Canvas {
        id: mycanvas
        x: screen.width + button_template.x + 10
        y: screen.y
        width: (button1_4.y + button1_4.height) - graphScreen.y
        height: (button1_4.y + button1_4.height) - graphScreen.y
        onPaint: {
            var ctx = getContext("2d");

            ctx.fillStyle = Qt.rgba(1, 1, 1, 1);
            ctx.fillRect(0, 0, width, height);

            drawLine(ctx,0,height/2,width,height/2,Qt.rgba(0,0,0,1),3);
            drawLine(ctx,width/2,0,width/2,height,Qt.rgba(0,0,0,1),3);


            var i = width/2;
            var offset = buttonGraph_plus.offset;

            for(; i < width; i=i+offset)
                drawLine(ctx,i,0,i,height,Qt.rgba(0,0,0,1),1);

            for(i = width/2; i > 0; i=i-offset)
                drawLine(ctx,i,0,i,height,Qt.rgba(0,0,0,1),1);

            for(i = height/2; i < height; i=i+offset)
                drawLine(ctx,0,i,width,i,Qt.rgba(0,0,0,1),1);

            for(i =  height/2; i > 0; i=i-offset)
                drawLine(ctx,0,i,width,i,Qt.rgba(0,0,0,1),1);


            mycanvas.drawGraph(ctx, Math.sin,offset);

        }

        function drawGraph(context,mathf,gap)
        {
            var i;
            var oldx,oldy;
            var offset = (mycanvas.width - mycanvas.width/2);
            var y;

            for (i = 0; i < offset; i++)
            {
                y = gap * (mathf(i/gap));
                y = -1*Math.round(y);
                if (i !== 0)
                    mycanvas.drawLine(context,oldx,oldy,i+mycanvas.width/2,y + mycanvas.height/2,Qt.rgba(1,0,0,1),3);

                oldx = i + mycanvas.width/2;
                oldy = y + mycanvas.height/2;
            }

        }

        function drawLine(context,fromX,fromY,toX,toY,rgba,line_width)
        {
            context.strokeStyle = rgba;
            context.lineWidth = line_width;
            context.beginPath();
            context.moveTo(fromX,fromY);
            context.lineTo(toX,toY);
            context.stroke();

        }
    }


    Rectangle{
       id: buttonGraph_plus
        x: buttonScience_6_2.x + buttonScience_6_2.width + 20
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.height
        height: button_template.height
        radius: 100
        color:  mouse_template36.containsMouse ?  mouse_template36.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        property int offset: 40

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template36
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {buttonGraph_plus.offset++; mycanvas.requestPaint();}

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "+"
            color: mouse_template36.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize + 6
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }

    Rectangle{
       id: buttonGraph_minus
        x: buttonGraph_plus.x + buttonGraph_plus.width + 20
        y: button1_5.y + button_template.height + button_template.x
        width: button_template.height
        height: button_template.height
        radius: 100
        color:  mouse_template37.containsMouse ?  mouse_template37.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }


        MouseArea{

            id: mouse_template37
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {buttonGraph_plus.offset--; mycanvas.requestPaint();}

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "-"
            color: mouse_template37.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize + 6
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


    Rectangle{
       id: buttonPrintGraph
        x: graphScreen.x + graphScreen.width - button_template.width;
        y: graphScreen.y + graphScreen.height + button_template.x;
        width: button_template.width
        height: button_template.height
        radius: 20
        color:  mouse_template38.containsMouse ?  mouse_template38.containsPress ? operation_button_click.color : operation_button_light.color : operation_button.color

        Behavior on color {
                    ColorAnimation { duration: root.animDuration }
                }

        MouseArea{

            id: mouse_template38
            anchors.fill: parent
            hoverEnabled: true

        }

        Text{

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: "Plot graph"
            color: mouse_template38.containsPress ? button_text_onClicked.color : button_text.color
            font.pointSize: button_text.font.pointSize
            font.family: button_text.font.family

            Behavior on color {
                        ColorAnimation { duration: root.animDuration }
                    }

        }

    }


}
