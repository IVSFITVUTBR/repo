/*!
 * \file tests.h.
 * \brief Kniznica implementujuca matematicke funkcie vyuzivajuce pri kalkulacke
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#include <iostream>
#include <math.h>
#include "tests.h"
#include "../calcsrc/matlib.h"


int main(void)
{
	TEST_EQ_FLOAT("test-1_rounddown", 10.0, rounddown(10.80))
	TEST_EQ_FLOAT("test-2_rounddown", 10.0, rounddown(10.40))
	TEST_EQ_FLOAT("test-3_rounddown", 0.0, rounddown(0.40))
	TEST_EQ_FLOAT("test-4_rounddown", 0.0, rounddown(0.50))
	TEST_EQ_FLOAT("test-5_rounddown", 20.0, rounddown(20.4999))

	TEST_EQ_FLOAT("test-1_plus", 10.0 + 0.1, plus(10.0, 0.1))
	TEST_EQ_FLOAT("test-2_plus", 10.0 + (-0.1), plus(10.0,-0.1))
	TEST_EQ_FLOAT("test-3_plus",-10.0 + 0.211, plus(-10.0,0.211))
	TEST_EQ_FLOAT("test-4_plus",(-10.0) + (-0.211), plus(-10.0,-0.211))

	TEST_EQ_FLOAT("test-1_minus", 10.0 - 0.1, minus(10.0, 0.1))
	TEST_EQ_FLOAT("test-2_minus", 10.0 - (-0.1), minus(10.0,-0.1))
	TEST_EQ_FLOAT("test-3_minus",-10.0 - 0.211, minus(-10.0,0.211))
	TEST_EQ_FLOAT("test-4_minus",(-10.0) - (-0.211), minus(-10.0,-0.211))

	TEST_EQ_FLOAT("test-1_divide", 10.0/10.0, divide(10.0, 10.0))
	TEST_EQ_FLOAT("test-2_divide", 2.458/2.458, divide(2.458, 2.458))
	TEST_EQ_FLOAT("test-3_divide", -10.0/-10.0, divide(-10.0, -10.0))
	TEST_EQ_FLOAT("test-5_divide_zero", 0.0/10.0, divide(0.0, 10.0))
	TEST_EQ_FLOAT("test-6_divide_zero", 50.0/0.0, divide(50.0, 0.0))
	TEST_EQ_FLOAT("test-7_divide", -10.0/10.0, divide(-10.0, 10.0))
	TEST_EQ_FLOAT("test-8_divide", 10.0/-10.0, divide(10.0, -10.0))
	TEST_EQ_FLOAT("test-9_divide", -5.455/10.688, divide(-5.455, 10.688))
	TEST_EQ_FLOAT("test-10_divide", 5.455/-10.688, divide(5.455, -10.688))

	TEST_EQ_FLOAT("test-1_multiply", 5.0*10.0, multiply(5.0, 10.0))
	TEST_EQ_FLOAT("test-2_multiply", 0.0*0.0, multiply(0.0, 0.0))
	TEST_EQ_FLOAT("test-3_multiply", -5.0*10.0, multiply(-5.0, 10.0))
	TEST_EQ_FLOAT("test-4_multiply", -5.4570*10.7770, multiply(-5.4570, 10.7770))
	TEST_EQ_FLOAT("test-5_multiply", 0.0*10.0, multiply(0.0, 10.0))
	TEST_EQ_FLOAT("test-6_multiply", 1.0*50.091919, multiply(1.0, 50.091919))
	TEST_EQ_FLOAT("test-7_multiply", -5.0*(-10.0), multiply(-5.0, -10.0))

	TEST_EQ_FLOAT("test-1_power", pow(2.0,3.0), power(2.0,3.0))
	TEST_EQ_FLOAT("test-3_power", pow(2.0,0.0), power(2.0,0.0))
	TEST_EQ_FLOAT("test-7_power", pow(2.0,1.0), power(2.0,1.0))
	TEST_EQ_FLOAT("test-8_power", pow(1000.0,1000.0), power(1000.0,1000.0))

	TEST_EQ_FLOAT("test-1_modulo", double(1 % 2), modulo(1.0,2.0))
	TEST_EQ_FLOAT("test-2_modulo", double(5 % 2), modulo(5.0,2.0))
	TEST_EQ_FLOAT("test-3_modulo", double(-1 % 2), modulo(-1.0,2.0))
	TEST_EQ_FLOAT("test-4_modulo", double(-5 % 2), modulo(-5.0,2.0))

	TEST_EQ_FLOAT("test-1_factorial", 120.0, factorial(5.0))
	TEST_EQ_FLOAT("test-2_factorial", NAN, factorial(-5.0))
	TEST_EQ_FLOAT("test-3_factorial", INFINITY, factorial(500.0))

	TEST_EQ_FLOAT("test-1_sqroot", sqrt(5.0), sqroot(5.0))
	TEST_EQ_FLOAT("test-2_sqroot", sqrt(-5.0), sqroot(-5.0))
	TEST_EQ_FLOAT("test-3_sqroot", sqrt(0.00012), sqroot(0.00012))
	TEST_EQ_FLOAT("test-4_sqroot", sqrt(1.0), sqroot(1.0))
	TEST_EQ_FLOAT("test-5_sqroot", sqrt(0.0), sqroot(0.0))

	TEST_EQ_FLOAT("test-1_frac", 1/2.0, frac(2.0))
	TEST_EQ_FLOAT("test-2_frac", 1/-2.0, frac(-2.0))
	TEST_EQ_FLOAT("test-3_frac", 1/100.0, frac(100.0))
	TEST_EQ_FLOAT("test-4_frac", 1/0.8780, frac(0.8780))
	TEST_EQ_FLOAT("test-5_frac", 1/0.000012, frac(0.000012))


	TEST_INFO("IVS CALC - 2018")
	return 0;
}