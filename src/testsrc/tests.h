/*!
 * \file tests.h.
 * \brief Kniznica implementujuca matematicke funkcie vyuzivajuce pri kalkulacke
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#ifndef TEST_H
#define TEST_H

#include <map>
#define ANSI_COLOR_RED    "\x1b[31m"
#define ANSI_COLOR_GREEN  "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_RESET  "\x1b[0m"


static int passed_tests = 0;
static int failed_tests = 0;
static std::map<std::string, int> hash_map;


#define TEST_EQ_FLOAT(test_name,result1,result2) 																					\
do 																																	\
{																																	\
	if (hash_map.find(std::string (test_name)) != hash_map.end())																	\
	{																																\
		printf(ANSI_COLOR_YELLOW "TEST WITH NAME \"%s\" ALREADY USED\n" ANSI_COLOR_RESET, test_name);								\
		return 1;																													\
	}																																\
	else																															\
	{																																\
		hash_map[std::string (test_name)] = 1;																						\
	}																																\
	if (result1 == result2)																											\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%f,%f) [%s]\n", test_name,result1,result2, ANSI_COLOR_GREEN "OK" ANSI_COLOR_RESET);		\
		passed_tests++;																												\
	}																																\
	else																															\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%f,%f) [%s]\n", test_name,result1,result2, ANSI_COLOR_RED "FAILED" ANSI_COLOR_RESET);	\
		failed_tests++;																												\
	}																																\
} while (0);


#define TEST_GT_FLOAT(test_name,result1,result2) 																					\
do 																																	\
{																																	\
	if (hash_map.find(std::string (test_name)) != hash_map.end())																	\
	{																																\
		printf(ANSI_COLOR_YELLOW "TEST WITH NAME \"%s\" ALREADY USED\n" ANSI_COLOR_RESET, test_name);								\
		return 1;																													\
	}																																\
	else																															\
	{																																\
		hash_map[std::string (test_name)] = 1; 																						\
	}																																\
	if (result1 > result2)																											\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%f,%f) [%s]\n", test_name,result1,result2, ANSI_COLOR_GREEN "OK" ANSI_COLOR_RESET);		\
		passed_tests++;																												\
	}																																\
	else																															\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%f,%f) [%s]\n", test_name,result1,result2, ANSI_COLOR_RED "FAILED" ANSI_COLOR_RESET);	\
		failed_tests++;																												\
	}																																\
} while (0);


#define TEST_LT_FLOAT(test_name,result1,result2) 																					\
do 																																	\
{																																	\
	if (hash_map.find(std::string (test_name)) != hash_map.end())																	\
	{																																\
		printf(ANSI_COLOR_YELLOW "TEST WITH NAME \"%s\" ALREADY USED\n" ANSI_COLOR_RESET, test_name);								\
		return 1;																													\
	}																																\
	else																															\
	{																																\
		hash_map[std::string (test_name)] = 1;																						\
	}																																\
																																	\
	if (result1 < result2)																											\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%f,%f) [%s]\n", test_name,result1,result2, ANSI_COLOR_GREEN "OK" ANSI_COLOR_RESET);		\
		passed_tests++;																												\
	}																																\
	else																															\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%f,%f) [%s]\n", test_name,result1,result2, ANSI_COLOR_RED "FAILED" ANSI_COLOR_RESET);	\
		failed_tests++;																												\
	}																																\
} while (0);


#define TEST_EQ_INT(test_name,result1,result2) 																						\
do 																																	\
{																																	\
	if (hash_map.find(std::string (test_name)) != hash_map.end())																	\
	{																																\
		printf(ANSI_COLOR_YELLOW "TEST WITH NAME \"%s\" ALREADY USED\n" ANSI_COLOR_RESET, test_name);								\
		return 1;																													\
	}																																\
	else																															\
	{																																\
		hash_map[std::string (test_name)] = 1;																						\
	}																																\
																																	\
	if (result1 == result2)																											\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%d,%d) [%s]\n", test_name,result1,result2, ANSI_COLOR_GREEN "OK" ANSI_COLOR_RESET);		\
		passed_tests++;																												\
	}																																\
	else																															\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%d,%d) [%s]\n", test_name,result1,result2, ANSI_COLOR_RED "FAILED" ANSI_COLOR_RESET);	\
		failed_tests++;																												\
	}																																\
} while (0);


#define TEST_GT_INT(test_name,result1,result2) 																						\
do 																																	\
{																																	\
	if (hash_map.find(std::string (test_name)) != hash_map.end())																	\
	{																																\
		printf(ANSI_COLOR_YELLOW "TEST WITH NAME \"%s\" ALREADY USED\n" ANSI_COLOR_RESET, test_name);								\
		return 1;																													\
	}																																\
	else																															\
	{																																\
		hash_map[std::string (test_name)] = 1;																						\
	}																																\
																																	\
	if (result1 > result2)																											\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%d,%d) [%s]\n", test_name,result1,result2, ANSI_COLOR_GREEN "OK" ANSI_COLOR_RESET);		\
		passed_tests++;																												\
	}																																\
	else																															\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%d,%d) [%s]\n", test_name,result1,result2, ANSI_COLOR_RED "FAILED" ANSI_COLOR_RESET);	\
		failed_tests++;																												\
	}																																\
} while (0);


#define TEST_LT_INT(test_name,result1,result2) 																						\
do 																																	\
{																																	\
	if (hash_map.find(std::string (test_name)) != hash_map.end())																	\
	{																																\
		printf(ANSI_COLOR_YELLOW "TEST WITH NAME \"%s\" ALREADY USED\n" ANSI_COLOR_RESET, test_name);								\
		return 1;																													\
	}																																\
	else																															\
	{																																\
		hash_map[std::string (test_name)] = 1;																						\
	}																																\
																																	\
	if (result1 == result2)																											\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%d,%d) [%s]\n", test_name,result1,result2, ANSI_COLOR_GREEN "OK" ANSI_COLOR_RESET);		\
		passed_tests++;																												\
	}																																\
	else																															\
	{																																\
		printf("TEST \"%s\" WITH ARGUMENTS (%d,%d) [%s]\n", test_name,result1,result2, ANSI_COLOR_RED "FAILED" ANSI_COLOR_RESET);	\
		failed_tests++;																												\
	}																																\
} while (0);


#define TEST_INFO(tested_project_name)													\
do   																					\
{																						\
	printf("PROJECT: %s\n", tested_project_name);										\
	printf("TESTS PASSED: %d/%d\n",passed_tests,passed_tests+failed_tests);				\
	printf("TESTS FAILED: %d/%d\n",failed_tests, failed_tests+passed_tests);       		\
	if (passed_tests + failed_tests == 0)												\
		printf("SUCCES RATE: %.2f\n",100*(float)0);											 \
	else																					 \
		printf("SUCCES RATE: %.2f\n",100*(float(passed_tests)/(passed_tests+failed_tests))); \
} while (0);																			\


#endif