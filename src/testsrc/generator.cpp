/*!
 * \file generator.cpp.
 * \brief Kniznica implementujuca matematicke funkcie vyuzivajuce pri kalkulacke
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#define DEFAULT_LOW 0
#define DEFAULT_HIGH 100
#define DEFAULT_CNT 10

int main(int argc, char const *argv[])
{
	int lowerLimit = DEFAULT_LOW;
	int upperLimit = DEFAULT_HIGH;
	int count = DEFAULT_CNT;
	std::string fname = "inputN";
	FILE * file;
	int c;

	while ((c = getopt(argc, (char* const *)argv, (const char*)"l:u:c:o:")) != -1)
    switch (c)
      {
      case 'l':
        lowerLimit = std::stoi(std::string(optarg),0);
        break;
      case 'u':
        upperLimit = std::stoi(std::string(optarg),0);
        break;
      case 'c':
        count = std::stoi(std::string(optarg),0);
        break;
      case 'o':
      	fname = std::string(optarg);
      	break;
      case '?':
      	printf("%s\n", "-l lowerLimit -u upperLimit -c count -o output");
      	return 1;
      default:
        abort ();
      }

	srand(time(NULL));
	file = fopen(fname.c_str(),"w");
	int r;
	
	for (int i = 0; i < count ; i++)
	{
		r = lowerLimit + rand() % (upperLimit - lowerLimit);
		fprintf(file, "%.2f\n",(double)r);
	}

	fclose(file);

	return 0;
}