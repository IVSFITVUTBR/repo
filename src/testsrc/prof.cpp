/*!
 * \file prof.cpp.
 * \brief Kniznica implementujuca matematicke funkcie vyuzivajuce pri kalkulacke
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#include <iostream>
#include <stdio.h>
#include <math.h>
#include <vector>
#include "../calcsrc/matlib.h"

double std_deviation(std::vector<double> data)
{
	double xavg = 0.0;
	double sum = 0.0;

 	// average
	for (unsigned int i = 0; i < data.size(); i++)
		xavg = plus(xavg,data[i]);

	xavg = divide(xavg,data.size());

	for (unsigned int i = 0; i < data.size(); i++)
		sum = plus(sum,power(data[i],2));

	sum = minus(sum,multiply(data.size(),power(xavg,2)));
	sum = sqroot(multiply(frac(minus(data.size(), 1)),sum));

	return sum;
}

int main(void)
{
	float flt;
	std::vector<double> input;

	while(EOF != fscanf(stdin,"%f",&flt))
		input.push_back(flt);

	printf("%d\n",(int)input.size());

	printf("%f\n", std_deviation(input));

	return 0;
}