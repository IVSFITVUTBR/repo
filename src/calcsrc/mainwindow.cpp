/*!
 * \file mainwindow.cpp.
 * \brief Subor popisujuci chovanie help okna.
 *  
 * Chovanie hlavneho okna kalkulacky. Reakcie na eventy
 * Volanie funkcii z matematickej kniznice
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QFont>
#include <QDebug>
#include <QList>
#include "matlib.h"
#include "help.h"


/*!
 * \brief Konstruktor hlavneho okna. Inicializuje niektore tlacitka so specialnymi znakmi. Nastaví styl okna a inicializuje displej na 0.
 */
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFont font = ui->buttonSqrt->font();
    font.setPointSize(18);
    ui->buttonSqrt->setText(QString::fromUtf8("\u221ax"));
    ui->buttonSqrt->setFont(font);

    ui->buttoDeleteNum->setText(QString::fromUtf8("\u232b"));
    ui->buttonMul->setText(QString::fromUtf8("\u2217"));
    ui->buttonPower->setText(QString::fromUtf8("x\u207F"));
    ui->buttonAbs->setText(QString::fromUtf8("1\u2044x"));

    // Make the result display box non-selectable with mouse
    ui->lineEdit->setDisabled(true);

    // Set the icon
    QIcon icon("ivscalc.svg");
    this->setWindowIcon(icon);


    setCustomStyle();
    // Set zero to the display on startup
    redraw(0);

}
/*!
 * \brief Destruktor hlavneho okna.
 */
MainWindow::~MainWindow()
{
    delete ui;
}
/*!
 * \brief Funkcia, ktora upravi dizajn okna.
 */
void MainWindow::setCustomStyle()
{
    this->setStyleSheet(R"(* {background: #484848;color: #DDDDDD;border: 1px solid #5A5A5A;}QWidget::item:selected {background: #191919;color: #FFFFFF;}QAbstractItemView {show-decoration-selected: 1;selection-background-color: #3D7848;selection-color: #DDDDDD;alternate-background-color: #353535;}QHeaderView {border: 1px solid #5A5A5A;}QHeaderView::section {background: #191919;border: 1px solid #5A5A5A;padding: 4px;}QHeaderView::section:selected, QHeaderView::section::checked {background: #353535;}QLabel {border: none;}QMenu::separator {background: #353535;}QLineEdit {border-radius: 5px;border: 3px solid #191919;background: #5A5A5A;})");


    for (QPushButton * item : this->findChildren<QPushButton *>()){
        item->setStyleSheet(R"(QPushButton {border: 0;border-radius: 3px;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #353535, stop: 1 #191919);}QPushButton:pressed {background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #191919, stop: 1 #353535);})");
    }

}

/*!
 * \brief Reakcia na stlacenie tlacidla 0
 */
void MainWindow::on_button0_clicked()
{
    getNumber(0);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 1
 */
void MainWindow::on_button1_clicked()
{
    getNumber(1);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 2
 */
void MainWindow::on_button2_clicked()
{
    getNumber(2);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 3
 */
void MainWindow::on_button3_clicked()
{
    getNumber(3);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 6
 */
void MainWindow::on_button6_clicked()
{
    getNumber(6);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 5
 */
void MainWindow::on_button5_clicked()
{
    getNumber(5);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 4
 */
void MainWindow::on_button4_clicked()
{
    getNumber(4);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 7
 */
void MainWindow::on_button7_clicked()
{
    getNumber(7);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 8
 */
void MainWindow::on_button8_clicked()
{
    getNumber(8);
}
/*!
 * \brief Reakcia na stlacenie tlacidla 9
 */
void MainWindow::on_button9_clicked()
{
    getNumber(9);
}
/*!
 * \brief Funkcia prekresli displej novym cislom
 * \param Cislo, ktore sa ma prekreslit
 */
void MainWindow::redraw(double number){
    QString num = QString::number(number, 'g', 16);
    if(num.length() < 17){
        ui->lineEdit->setText(num);
    }
    else{
        num=QString::number(number);
        ui->lineEdit->setText(num);
    }
}
/*!
 * \brief Reakcia na stlacenie tlacidla pre zmazanie vysledku.
 * Zmaze sa vysledok a na displej sa zobrazi nula.
 */
void MainWindow::on_buttonDelete_clicked()
{
    state=operand1;
    oper = noOp;
    redraw(0);
    comma=false;
    commaMul = 0.1;
    globalOperand1 = 0;
    globalOperand2 = 0;
}
/*!
 * \brief Reakcia na stlacenie tlacidla pre zmazanie posledneho cisla
 */
void MainWindow::on_buttoDeleteNum_clicked()
{
    if(state==operand1){
        if(comma){
            globalOperand1 = 0;
            comma=false;
            commaMul = 0.1;
            redraw(globalOperand1);
            return;
        }
        globalOperand1 /= 10;
        globalOperand1 = floor(globalOperand1);
        redraw(globalOperand1);
    }
    if(state==operand2){
        if(comma){
            comma=false;
            commaMul = 0.1;
            globalOperand2 = 0;
            redraw(globalOperand2);
            return;
        }
        globalOperand2 /= 10;
        globalOperand2 = floor(globalOperand2);
        redraw(globalOperand2);
    }
}
/*!
 * \brief Reakcia na stlacenie tlacidla rovna sa.
 * Vypocita vysledok podla zvolenej operacie
 */
void MainWindow::on_buttonEql_clicked()
{
    comma=false;
    commaMul = 0.1;
    calculate(oper);
}
/*!
 * \brief Vypocita vysledok podla zvolenej operacie a zadanych operandov
 * \param Zvolena operacia
 */
void MainWindow::calculate(operators localOper){
    switch(localOper){
    case noOp:
        state=result;
        break;
    case opPlus:
        globalOperand1 = plus(globalOperand1, globalOperand2);
        redraw(globalOperand1);
        state=result;
        break;
    case opMinus:
        globalOperand1 = minus(globalOperand1, globalOperand2);
        redraw(globalOperand1);
        state=result;
        break;
    case opMul:
        globalOperand1 = multiply(globalOperand1, globalOperand2);
        redraw(globalOperand1);
        state=result;
        break;
    case opDiv:
        globalOperand1 = divide(globalOperand1, globalOperand2);
        redraw(globalOperand1);
        state=result;
        break;
    case opPower:
        globalOperand1 = power(globalOperand1, globalOperand2);
        redraw(globalOperand1);
        state=result;
        break;
    case opFact:
        globalOperand1 = factorial(globalOperand1);
        redraw(globalOperand1);
        state=result;
        break;
    case opMod:
        globalOperand1 = modulo(globalOperand1, globalOperand2);
        redraw(globalOperand1);
        state=result;
        break;
    case opSqrt:
        globalOperand1 = sqroot(globalOperand1);
        redraw(globalOperand1);
        state=result;
        break;
    case opFrac:
        globalOperand1 = frac(globalOperand1);
        redraw(globalOperand1);
        state=result;
        break;
    default:
        break;
    }

}
/*!
 * \brief Sluzi k zadavaniu operandov.
 * \param Stlacene cislo.
 */
void MainWindow::getNumber(int number)
{
    if(state == operand1){
        if(comma){
            globalOperand1 = globalOperand1 + (commaMul * number);
            commaMul *= 0.1;
            redraw(globalOperand1);
            return;
        }
        globalOperand1 = globalOperand1 * 10 + number;
        redraw(globalOperand1);
    }
    if(state == operand2){
        if(comma){
            globalOperand2 = globalOperand2 + (commaMul * number);
            commaMul *= 0.1;
            redraw(globalOperand2);
            return;
        }
        globalOperand2 = globalOperand2 * 10 + number;
        redraw(globalOperand2);
    }
    if(state == result){
        state = operand1;
        oper=noOp;
        globalOperand1 = 0;
        globalOperand1 = globalOperand1 * 10 + number;
        redraw(globalOperand1);
    }
}
/*!
 * \brief Sluzi k zadavaniu operandov z klavesnice.
 * \param Udalost stlacenia klavesy
 */
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key()){
    case Qt::Key_0:
        ui->button0->animateClick();
        break;
    case Qt::Key_1:
        ui->button1->animateClick();
        break;
    case Qt::Key_2:
        ui->button2->animateClick();
        break;
    case Qt::Key_3:
        ui->button3->animateClick();
        break;
    case Qt::Key_4:
        ui->button4->animateClick();
        break;
    case Qt::Key_5:
        ui->button5->animateClick();
        break;
    case Qt::Key_6:
        ui->button6->animateClick();
        break;
    case Qt::Key_7:
        ui->button7->animateClick();
        break;
    case Qt::Key_8:
        ui->button8->animateClick();
        break;
    case Qt::Key_9:
        ui->button9->animateClick();
        break;
    case Qt::Key_Plus:
        ui->buttonAdd->animateClick();
        break;
    case Qt::Key_Minus:
        ui->buttonSub->animateClick();
        break;
    case 0x2A:
        ui->buttonMul->animateClick();
        break;
    case 0x2F:
        ui->buttonDiv->animateClick();
        break;
    case Qt::Key_M:
        ui->buttonMod->animateClick();
        break;
    case Qt::Key_Enter:
        ui->buttonEql->animateClick();
        break;
    case Qt::Key_Return:
        ui->buttonEql->animateClick();
        break;
    case 0x2C:
        ui->buttonComma->animateClick();
        break;
    case Qt::Key_Delete:
        ui->buttonDelete->animateClick();
        break;
    case Qt::Key_Backspace:
        ui->buttoDeleteNum->animateClick();
        break;
    }

}
/*!
 * \brief Pri zvoleni operacie prida znak na koniec cisla, aby uzivatel vedel, aku operaciu ma zvolenu
 * \param Znak operacie, ktory sa ma pridat.
 */
void MainWindow::redrawOperation(char operation)
{
    QString text = ui->lineEdit->text();
    if(text[text.length()-1] != '+' && text[text.length()-1] != '*' && text[text.length()-1] != '-' && text[text.length()-1] != '/' && text[text.length()-1] != 'M'){
        ui->lineEdit->setText(text.append(operation));
    }
    else{
        text[text.length()-1] = operation;
        ui->lineEdit->setText(text);
    }
}
/*!
 * \brief Reakcia na stlacenie klavesy plus. 
 */
void MainWindow::on_buttonAdd_clicked()
{
    redrawOperation('+');
    if(state==operand1){
        state = operand2;
        globalOperand2 = 0;
        oper = opPlus;
        comma=false;
        commaMul = 0.1;
        return;
    }
    if(state==operand2){
        if(oper != opPlus){
            oper = opPlus;
            return;
        }
        calculate(oper);
        globalOperand2=0;
        state=operand2;
        comma=false;
        commaMul = 0.1;
        redrawOperation('+');
    }
    if(state==result){
        oper = opPlus;
        globalOperand2 = 0;
        state=operand2;
    }

}
/*!
 * \brief Reakcia na stlacenie tlacitka minus. 
 */
void MainWindow::on_buttonSub_clicked()
{
    redrawOperation('-');
    if(state==operand1){
        state = operand2;
        globalOperand2 = 0;
        oper = opMinus;
        comma=false;
        commaMul = 0.1;
        return;
    }
    if(state==operand2){
        if(oper != opMinus){
            oper = opMinus;
            return;
        }
        calculate(oper);
        globalOperand2=0;
        redrawOperation('-');
        state=operand2;
        comma=false;
        commaMul = 0.1;
    }
    if(state==result){
        oper = opMinus;
        globalOperand2 = 0;
        state=operand2;
    }

}

/*!
 * \brief Reakcia na stlacenie tlacitka nasobenia. 
 */
void MainWindow::on_buttonMul_clicked()
{
    redrawOperation('*');
    if(state==operand1){
        state = operand2;
        globalOperand2 = 0;
        oper = opMul;
        comma=false;
        commaMul = 0.1;
        return;
    }
    if(state==operand2){
        if(oper != opMul){
            oper = opMul;
            return;
        }
        calculate(oper);
        globalOperand2=0;
        state=operand2;
        redrawOperation('*');
        comma=false;
        commaMul = 0.1;
    }
    if(state==result){
        oper = opMul;
        globalOperand2 = 0;
        state=operand2;
    }
}
/*!
 * \brief Reakcia na stlacenie tlacitka deleno. 
 */
void MainWindow::on_buttonDiv_clicked()
{
    redrawOperation('/');
    if(state==operand1){
        state = operand2;
        globalOperand2 = 0;
        oper = opDiv;
        comma=false;
        commaMul = 0.1;
        return;
    }
    if(state==operand2){
        if(oper!= opDiv){
            oper = opDiv;
            return;
        }
        calculate(oper);
        globalOperand2=0;
        state=operand2;
        redrawOperation('/');
        comma=false;
        commaMul = 0.1;
    }
    if(state==result){
        oper = opDiv;
        globalOperand2 = 0;
        state=operand2;
    }

}
/*!
 * \brief Reakcia na stlacenie tlacitka mocniny. 
 */
void MainWindow::on_buttonPower_clicked()
{
    redrawOperation('^');
    if(state==operand1){
        state = operand2;
        globalOperand2 = 0;
        oper = opPower;
        comma=false;
        commaMul = 0.1;
        return;
    }
    if(state==operand2){
        if(oper!=opPower){
            oper = opPower;
            return;
        }
        calculate(oper);
        globalOperand2=0;
        state=operand2;
        redrawOperation('^');
        comma=false;
        commaMul = 0.1;
    }
    if(state==result){
        oper = opPower;
        globalOperand2 = 0;
        state=operand2;
    }
}

/*!
 * \brief Reakcia stlacenia tlacitka faktorial. 
 */
void MainWindow::on_buttonFactorial_clicked()
{
    oper = opFact;
    calculate(oper);
}
/*!
 * \brief Reakcia na stlacenie klavesy modulo. 
 */
void MainWindow::on_buttonMod_clicked()
{
    redrawOperation('m');
    if(state==operand1){
        state = operand2;
        globalOperand2 = 0;
        oper = opMod;
        comma=false;
        commaMul = 0.1;
        return;
    }
    if(state==operand2){
        if(oper != opMod){
            oper = opMod;
            return;
        }
        calculate(oper);
        globalOperand2=0;
        state=operand2;
        redrawOperation('m');
        comma=false;
        commaMul = 0.1;
    }
    if(state==result){
        oper = opMod;
        globalOperand2 = 0;
        state=operand2;
    }
}

void MainWindow::on_buttonAbs_clicked()
{
    oper = opFrac;
    calculate(oper);
}
/*!
 * \brief Reakcia na stlacenie klavesy odmocnina. 
 */
void MainWindow::on_buttonSqrt_clicked()
{
    oper = opSqrt;
    calculate(oper);
    comma=false;
    commaMul = 0.1;
}
/*!
 * \brief Reakcia na stlacenie klavesy desatinne cislo. 
 */
void MainWindow::on_buttonComma_clicked()
{
    if(state == operand2 && oper == opPower){
        ui->lineEdit->setText("Iba prirodzene");
        globalOperand1=0;
        globalOperand2=0;
        state = operand1;
        oper=noOp;
        return;
    }
    if(state == result){
        globalOperand1 = 0;
        state = operand1;
        redraw(globalOperand1);
    }
    if(state == operand2&&!comma){
        redraw(globalOperand2);
    }
    if(!comma){
        comma = true;
        commaMul = 0.1;
        ui->lineEdit->setText(ui->lineEdit->text().append("."));
    }
}
/*!
 * \brief Reakcia na stlacenie klavesy negacie. 
 */
void MainWindow::on_buttonNeg_clicked()
{
    if((state==operand1 || state==result)){
        globalOperand1 = -globalOperand1;
        redraw(globalOperand1);
    }
    else{
        globalOperand2 = -globalOperand2;
        redraw(globalOperand2);
    }

}
/*!
 * \brief Reakcia na stlacenie moznosti exit 
 */
void MainWindow::on_actionExit_triggered()
{
    this->close();
}
/*!
 * \brief Reakcia na stlacenie moznosti help. 
 */
void MainWindow::on_actionHelp_triggered()
{
    Help * wind = new Help();
    wind->show();
}
