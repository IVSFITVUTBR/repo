/********************************************************************************
** Form generated from reading UI file 'help.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HELP_H
#define UI_HELP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Help
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QGridLayout *gridLayout;
    QLabel *label_5;
    QLabel *label_7;
    QLabel *label_6;
    QLabel *label_4;
    QLabel *label_9;
    QLabel *label_8;
    QLabel *label_10;
    QLabel *label_11;

    void setupUi(QWidget *Help)
    {
        if (Help->objectName().isEmpty())
            Help->setObjectName(QStringLiteral("Help"));
        Help->resize(513, 446);
        QPalette palette;
        QBrush brush(QColor(53, 53, 53, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        QBrush brush1(QColor(164, 166, 168, 96));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        Help->setPalette(palette);
        Help->setStyleSheet(QLatin1String("QWidget {\n"
"		background: #353535;\n"
"}\n"
"\n"
"QLabel {\n"
"		background: #353535;\n"
"}\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(Help);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(Help);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(16777215, 50));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI Black"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setFrameShape(QFrame::Box);
        label->setFrameShadow(QFrame::Raised);
        label->setWordWrap(false);

        verticalLayout->addWidget(label);

        label_2 = new QLabel(Help);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setEnabled(true);
        label_2->setMaximumSize(QSize(16777215, 150));
        QFont font1;
        font1.setFamily(QStringLiteral("Microsoft PhagsPa"));
        font1.setPointSize(18);
        font1.setBold(false);
        font1.setWeight(50);
        label_2->setFont(font1);
        label_2->setFrameShape(QFrame::NoFrame);
        label_2->setWordWrap(false);

        verticalLayout->addWidget(label_2, 0, Qt::AlignHCenter);

        label_3 = new QLabel(Help);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(16777215, 50));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI Black"));
        font2.setPointSize(18);
        font2.setBold(true);
        font2.setWeight(75);
        label_3->setFont(font2);
        label_3->setLayoutDirection(Qt::LeftToRight);
        label_3->setFrameShape(QFrame::NoFrame);
        label_3->setFrameShadow(QFrame::Plain);
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_5 = new QLabel(Help);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font3;
        font3.setPointSize(14);
        label_5->setFont(font3);
        label_5->setIndent(15);

        gridLayout->addWidget(label_5, 0, 1, 1, 1);

        label_7 = new QLabel(Help);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font3);
        label_7->setIndent(15);

        gridLayout->addWidget(label_7, 1, 1, 1, 1);

        label_6 = new QLabel(Help);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font3);
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_6->setIndent(15);

        gridLayout->addWidget(label_6, 1, 0, 1, 1);

        label_4 = new QLabel(Help);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font3);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_4->setIndent(15);

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        label_9 = new QLabel(Help);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font3);
        label_9->setIndent(15);

        gridLayout->addWidget(label_9, 2, 1, 1, 1);

        label_8 = new QLabel(Help);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font3);
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_8->setIndent(15);

        gridLayout->addWidget(label_8, 3, 0, 1, 1);

        label_10 = new QLabel(Help);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font3);
        label_10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_10->setIndent(15);

        gridLayout->addWidget(label_10, 2, 0, 1, 1);

        label_11 = new QLabel(Help);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font3);
        label_11->setIndent(15);

        gridLayout->addWidget(label_11, 3, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(Help);

        QMetaObject::connectSlotsByName(Help);
    } // setupUi

    void retranslateUi(QWidget *Help)
    {
        Help->setWindowTitle(QApplication::translate("Help", "O projekte", 0));
        label->setText(QApplication::translate("Help", "<font color='White'>IVS Projekt 2 - KALKULA\304\214KA</font>", 0));
        label_2->setText(QApplication::translate("Help", "<html>\n"
"<head/>\n"
"<body>\n"
"<p><font color='White'>Daniel Florek</font></p>\n"
"<p><font color='White'>Martin Grn\303\241\304\215</font></p>\n"
"<p><font color='White'>Andrej Hu\304\215ko</font></p>\n"
"</body>\n"
"</html>", 0));
        label_3->setText(QApplication::translate("Help", "<font color='White'>Ovl\303\241danie Kl\303\241vesnicou</font>", 0));
        label_5->setText(QApplication::translate("Help", "<font color='White'>P\303\255sanie \304\215\303\255slic</font>", 0));
        label_7->setText(QApplication::translate("Help", "<font color='White'>V\303\275ber oper\303\241cie</font>", 0));
        label_6->setText(QApplication::translate("Help", "<font color='White'>[+]  [-]  [*]  [/]  [M]</font>", 0));
        label_4->setText(QApplication::translate("Help", "<font color='White'>0 - 9</font>", 0));
        label_9->setText(QApplication::translate("Help", "<font color='White'>Desatinn\303\251 \304\215\303\255slo</font>", 0));
        label_8->setText(QApplication::translate("Help", "<font color='White'>Enter</font>", 0));
        label_10->setText(QApplication::translate("Help", "<font color='White'>\304\214iarka</font>", 0));
        label_11->setText(QApplication::translate("Help", "<font color='White'>V\303\275sledok</font>", 0));
    } // retranslateUi

};

namespace Ui {
    class Help: public Ui_Help {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HELP_H
