/*!
 * \file main.cpp.
 * \brief Hlavna vetva programu
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#include "mainwindow.h"
#include <QApplication>
/*!
 * \brief Vytvorenie Qt aplikacie a zobrazenie hlavneho okna.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
