/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QAction *actionHelp;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *lineEdit;
    QGridLayout *gridLayout;
    QPushButton *buttonEql;
    QPushButton *buttonSub;
    QPushButton *buttonAdd;
    QPushButton *buttonComma;
    QPushButton *buttonDiv;
    QPushButton *button2;
    QPushButton *button7;
    QPushButton *button1;
    QPushButton *button8;
    QPushButton *button5;
    QPushButton *button3;
    QPushButton *button6;
    QPushButton *button4;
    QPushButton *button9;
    QPushButton *button0;
    QPushButton *buttonPower;
    QPushButton *buttonMul;
    QPushButton *buttoDeleteNum;
    QPushButton *buttonNeg;
    QPushButton *buttonDelete;
    QPushButton *buttonAbs;
    QPushButton *buttonMod;
    QPushButton *buttonFactorial;
    QPushButton *buttonSqrt;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(429, 496);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionHelp = new QAction(MainWindow);
        actionHelp->setObjectName(QStringLiteral("actionHelp"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy);
        lineEdit->setMaximumSize(QSize(16777215, 70));
        QFont font;
        font.setPointSize(28);
        lineEdit->setFont(font);
        lineEdit->setMaxLength(16);
        lineEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEdit->setReadOnly(true);

        verticalLayout_2->addWidget(lineEdit);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonEql = new QPushButton(centralWidget);
        buttonEql->setObjectName(QStringLiteral("buttonEql"));
        sizePolicy.setHeightForWidth(buttonEql->sizePolicy().hasHeightForWidth());
        buttonEql->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(false);
        font1.setWeight(50);
        buttonEql->setFont(font1);
        buttonEql->setFlat(false);

        gridLayout->addWidget(buttonEql, 7, 3, 1, 1);

        buttonSub = new QPushButton(centralWidget);
        buttonSub->setObjectName(QStringLiteral("buttonSub"));
        sizePolicy.setHeightForWidth(buttonSub->sizePolicy().hasHeightForWidth());
        buttonSub->setSizePolicy(sizePolicy);
        buttonSub->setFont(font1);

        gridLayout->addWidget(buttonSub, 5, 3, 1, 1);

        buttonAdd = new QPushButton(centralWidget);
        buttonAdd->setObjectName(QStringLiteral("buttonAdd"));
        sizePolicy.setHeightForWidth(buttonAdd->sizePolicy().hasHeightForWidth());
        buttonAdd->setSizePolicy(sizePolicy);
        buttonAdd->setFont(font1);

        gridLayout->addWidget(buttonAdd, 6, 3, 1, 1);

        buttonComma = new QPushButton(centralWidget);
        buttonComma->setObjectName(QStringLiteral("buttonComma"));
        sizePolicy.setHeightForWidth(buttonComma->sizePolicy().hasHeightForWidth());
        buttonComma->setSizePolicy(sizePolicy);
        buttonComma->setFont(font1);

        gridLayout->addWidget(buttonComma, 7, 2, 1, 1);

        buttonDiv = new QPushButton(centralWidget);
        buttonDiv->setObjectName(QStringLiteral("buttonDiv"));
        sizePolicy.setHeightForWidth(buttonDiv->sizePolicy().hasHeightForWidth());
        buttonDiv->setSizePolicy(sizePolicy);
        buttonDiv->setFont(font1);

        gridLayout->addWidget(buttonDiv, 2, 3, 1, 1);

        button2 = new QPushButton(centralWidget);
        button2->setObjectName(QStringLiteral("button2"));
        sizePolicy.setHeightForWidth(button2->sizePolicy().hasHeightForWidth());
        button2->setSizePolicy(sizePolicy);
        button2->setFont(font1);

        gridLayout->addWidget(button2, 6, 1, 1, 1);

        button7 = new QPushButton(centralWidget);
        button7->setObjectName(QStringLiteral("button7"));
        sizePolicy.setHeightForWidth(button7->sizePolicy().hasHeightForWidth());
        button7->setSizePolicy(sizePolicy);
        button7->setFont(font1);

        gridLayout->addWidget(button7, 3, 0, 1, 1);

        button1 = new QPushButton(centralWidget);
        button1->setObjectName(QStringLiteral("button1"));
        sizePolicy.setHeightForWidth(button1->sizePolicy().hasHeightForWidth());
        button1->setSizePolicy(sizePolicy);
        button1->setFont(font1);

        gridLayout->addWidget(button1, 6, 0, 1, 1);

        button8 = new QPushButton(centralWidget);
        button8->setObjectName(QStringLiteral("button8"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(button8->sizePolicy().hasHeightForWidth());
        button8->setSizePolicy(sizePolicy1);
        button8->setFont(font1);

        gridLayout->addWidget(button8, 3, 1, 1, 1);

        button5 = new QPushButton(centralWidget);
        button5->setObjectName(QStringLiteral("button5"));
        sizePolicy.setHeightForWidth(button5->sizePolicy().hasHeightForWidth());
        button5->setSizePolicy(sizePolicy);
        button5->setFont(font1);

        gridLayout->addWidget(button5, 5, 1, 1, 1);

        button3 = new QPushButton(centralWidget);
        button3->setObjectName(QStringLiteral("button3"));
        sizePolicy.setHeightForWidth(button3->sizePolicy().hasHeightForWidth());
        button3->setSizePolicy(sizePolicy);
        button3->setFont(font1);

        gridLayout->addWidget(button3, 6, 2, 1, 1);

        button6 = new QPushButton(centralWidget);
        button6->setObjectName(QStringLiteral("button6"));
        sizePolicy.setHeightForWidth(button6->sizePolicy().hasHeightForWidth());
        button6->setSizePolicy(sizePolicy);
        button6->setFont(font1);

        gridLayout->addWidget(button6, 5, 2, 1, 1);

        button4 = new QPushButton(centralWidget);
        button4->setObjectName(QStringLiteral("button4"));
        sizePolicy.setHeightForWidth(button4->sizePolicy().hasHeightForWidth());
        button4->setSizePolicy(sizePolicy);
        button4->setFont(font1);

        gridLayout->addWidget(button4, 5, 0, 1, 1);

        button9 = new QPushButton(centralWidget);
        button9->setObjectName(QStringLiteral("button9"));
        sizePolicy.setHeightForWidth(button9->sizePolicy().hasHeightForWidth());
        button9->setSizePolicy(sizePolicy);
        button9->setFont(font1);

        gridLayout->addWidget(button9, 3, 2, 1, 1);

        button0 = new QPushButton(centralWidget);
        button0->setObjectName(QStringLiteral("button0"));
        sizePolicy.setHeightForWidth(button0->sizePolicy().hasHeightForWidth());
        button0->setSizePolicy(sizePolicy);
        button0->setFont(font1);

        gridLayout->addWidget(button0, 7, 1, 1, 1);

        buttonPower = new QPushButton(centralWidget);
        buttonPower->setObjectName(QStringLiteral("buttonPower"));
        sizePolicy.setHeightForWidth(buttonPower->sizePolicy().hasHeightForWidth());
        buttonPower->setSizePolicy(sizePolicy);
        buttonPower->setFont(font1);

        gridLayout->addWidget(buttonPower, 0, 3, 1, 1);

        buttonMul = new QPushButton(centralWidget);
        buttonMul->setObjectName(QStringLiteral("buttonMul"));
        sizePolicy.setHeightForWidth(buttonMul->sizePolicy().hasHeightForWidth());
        buttonMul->setSizePolicy(sizePolicy);
        buttonMul->setFont(font1);

        gridLayout->addWidget(buttonMul, 3, 3, 1, 1);

        buttoDeleteNum = new QPushButton(centralWidget);
        buttoDeleteNum->setObjectName(QStringLiteral("buttoDeleteNum"));
        sizePolicy.setHeightForWidth(buttoDeleteNum->sizePolicy().hasHeightForWidth());
        buttoDeleteNum->setSizePolicy(sizePolicy);
        buttoDeleteNum->setFont(font1);

        gridLayout->addWidget(buttoDeleteNum, 2, 1, 1, 1);

        buttonNeg = new QPushButton(centralWidget);
        buttonNeg->setObjectName(QStringLiteral("buttonNeg"));
        sizePolicy.setHeightForWidth(buttonNeg->sizePolicy().hasHeightForWidth());
        buttonNeg->setSizePolicy(sizePolicy);
        buttonNeg->setFont(font1);

        gridLayout->addWidget(buttonNeg, 7, 0, 1, 1);

        buttonDelete = new QPushButton(centralWidget);
        buttonDelete->setObjectName(QStringLiteral("buttonDelete"));
        sizePolicy.setHeightForWidth(buttonDelete->sizePolicy().hasHeightForWidth());
        buttonDelete->setSizePolicy(sizePolicy);
        buttonDelete->setFont(font1);

        gridLayout->addWidget(buttonDelete, 2, 0, 1, 1);

        buttonAbs = new QPushButton(centralWidget);
        buttonAbs->setObjectName(QStringLiteral("buttonAbs"));
        sizePolicy.setHeightForWidth(buttonAbs->sizePolicy().hasHeightForWidth());
        buttonAbs->setSizePolicy(sizePolicy);
        QFont font2;
        font2.setPointSize(18);
        buttonAbs->setFont(font2);

        gridLayout->addWidget(buttonAbs, 0, 1, 1, 1);

        buttonMod = new QPushButton(centralWidget);
        buttonMod->setObjectName(QStringLiteral("buttonMod"));
        sizePolicy.setHeightForWidth(buttonMod->sizePolicy().hasHeightForWidth());
        buttonMod->setSizePolicy(sizePolicy);
        buttonMod->setFont(font2);
        buttonMod->setStyleSheet(QStringLiteral(""));

        gridLayout->addWidget(buttonMod, 0, 0, 1, 1);

        buttonFactorial = new QPushButton(centralWidget);
        buttonFactorial->setObjectName(QStringLiteral("buttonFactorial"));
        sizePolicy.setHeightForWidth(buttonFactorial->sizePolicy().hasHeightForWidth());
        buttonFactorial->setSizePolicy(sizePolicy);
        buttonFactorial->setFont(font1);

        gridLayout->addWidget(buttonFactorial, 2, 2, 1, 1);

        buttonSqrt = new QPushButton(centralWidget);
        buttonSqrt->setObjectName(QStringLiteral("buttonSqrt"));
        sizePolicy.setHeightForWidth(buttonSqrt->sizePolicy().hasHeightForWidth());
        buttonSqrt->setSizePolicy(sizePolicy);
        QFont font3;
        font3.setBold(false);
        font3.setWeight(50);
        buttonSqrt->setFont(font3);

        gridLayout->addWidget(buttonSqrt, 0, 2, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 429, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionHelp);
        menuFile->addAction(actionExit);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "IVS Kalkula\304\215ka", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Ukon\304\215i\305\245", 0));
        actionHelp->setText(QApplication::translate("MainWindow", "Pomocn\303\255k", 0));
        buttonEql->setText(QApplication::translate("MainWindow", "=", 0));
        buttonSub->setText(QApplication::translate("MainWindow", "-", 0));
        buttonAdd->setText(QApplication::translate("MainWindow", "+", 0));
        buttonComma->setText(QApplication::translate("MainWindow", ",", 0));
        buttonDiv->setText(QApplication::translate("MainWindow", "/", 0));
        button2->setText(QApplication::translate("MainWindow", "2", 0));
        button7->setText(QApplication::translate("MainWindow", "7", 0));
        button1->setText(QApplication::translate("MainWindow", "1", 0));
        button8->setText(QApplication::translate("MainWindow", "8", 0));
        button5->setText(QApplication::translate("MainWindow", "5", 0));
        button3->setText(QApplication::translate("MainWindow", "3", 0));
        button6->setText(QApplication::translate("MainWindow", "6", 0));
        button4->setText(QApplication::translate("MainWindow", "4", 0));
        button9->setText(QApplication::translate("MainWindow", "9", 0));
        button0->setText(QApplication::translate("MainWindow", "0", 0));
        buttonPower->setText(QApplication::translate("MainWindow", "x^y", 0));
        buttonMul->setText(QApplication::translate("MainWindow", "*", 0));
        buttoDeleteNum->setText(QApplication::translate("MainWindow", "DEL LN", 0));
        buttonNeg->setText(QApplication::translate("MainWindow", "\302\261", 0));
        buttonDelete->setText(QApplication::translate("MainWindow", "C", 0));
        buttonAbs->setText(QApplication::translate("MainWindow", "1/x", 0));
        buttonMod->setText(QApplication::translate("MainWindow", "MOD", 0));
        buttonFactorial->setText(QApplication::translate("MainWindow", "x!", 0));
        buttonSqrt->setText(QString());
        menuFile->setTitle(QApplication::translate("MainWindow", "Ponuka", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
