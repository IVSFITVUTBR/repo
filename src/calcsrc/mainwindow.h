/*!
 * \class MainWindow
 * \file mainwindow.h.
 * \brief Deklaracia triedy hlavneho okna.
 * Hlavne okno starajuce sa o vyzor a funkcionalitu kalkulacky.
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <math.h>
#include <QKeyEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
    * \brief Konstruktor okna.
    */
    explicit MainWindow(QWidget *parent = 0);
    /*!
    * \brief Destruktor okna.
    */
    ~MainWindow();
    /*!
    * \brief Enumeracia operacii, ktore dokaze kalkulacka spracovat.
    */
    enum operators{noOp, opPlus, opMinus, opDiv, opMul, opSqrt, opPower, opMod, opFact, opLog, opFrac};
    /*!
    * \brief Stavy kalkulacky, urcuju ktory operand sa ma nacitavat
    */
    enum states{operand1, operand2, result};


private slots:
    /*!
    * \brief Reakcia na stlacenie tlacidla 0.
    */
    void on_button0_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 1.
    */
    void on_button1_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 2.
    */
    void on_button2_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 3.
    */
    void on_button3_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 6.
    */
    void on_button6_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 5.
    */
    void on_button5_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 4.
    */
    void on_button4_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 7.
    */
    void on_button7_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 8.
    */
    void on_button8_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla 9.
    */
    void on_button9_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla zmazat vysledok.
    */
    void on_buttonDelete_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla zmazat posledne cislo.
    */
    void on_buttoDeleteNum_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla rovna sa.
    */
    void on_buttonEql_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla plus.
    */
    void on_buttonAdd_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla minus.
    */
    void on_buttonSub_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla nasobenie.
    */
    void on_buttonMul_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla delenie
    */
    void on_buttonDiv_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla mocnina.
    */
    void on_buttonPower_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla factorial.
    */
    void on_buttonFactorial_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla modulo.
    */
    void on_buttonMod_clicked();

    void on_buttonAbs_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla odmocnina.
    */
    void on_buttonSqrt_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla desatinne cislo.
    */
    void on_buttonComma_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla negacie.
    */
    void on_buttonNeg_clicked();
    /*!
    * \brief Reakcia na stlacenie tlacidla exit.
    */
    void on_actionExit_triggered();
    /*!
    * \brief Reakcia na stlacenie tlacidla help.
    */
    void on_actionHelp_triggered();
    /*!
    * \brief Pomocna funkcia, ktora nastavuje vzhlad kalkulacky.
    */
    void setCustomStyle();

private:
    Ui::MainWindow *ui;
    /*!
    * \brief Pomocna funkcia prekreslujuca zobrazovane cislo v kalkulacke.
    */
    void redraw(double number);
    /*!
    * \brief Premenna ktora urcuje, ci sa nacitava desatinne cislo alebo nie.
    */
    bool comma = false;
    /*!
    * \brief Premenna obsahujúca nasobok, pri desatinnom cisle
    */
    double commaMul = 0.1;
    /*!
    * \brief Premenna obsahujuca vybranu operaciu.
    */
    operators oper=noOp;
    /*!
    * \brief Prvy operand operacie.
    */
    double globalOperand1 = 0;
    /*!
    * \brief Druhy operand operacie.
    */
    double globalOperand2 = 0;
    /*!
    * \brief Premenna urcujuca stav, ci sa nacitava prvy, ci druhy operand, alebo ci sa nachadza v stave vysledku.
    */    
    states state = operand1;
    /*!
    * \brief Funkcia ktora vykona zvolenu operaciu nad zadanymi operandami.
    */
    void calculate(MainWindow::operators oper);
    /*!
    * \brief Nacita cislo.
    */
    void getNumber(int number);
    /*!
    * \brief Osetrenie klavesnicovych vstupov.
    */
    void keyPressEvent(QKeyEvent * event);
    /*!
    * \brief Dopise vybranu operaciu na koniec operandu.
    */
    void redrawOperation(char operation);
};

#endif // MAINWINDOW_H
