/*!
 * \file help.cpp.
 * \brief Subor popisujuci chovanie help okna.
 *
 * Spravanie HELP okna je jednoduche. Iba vypisuje ovladanie kalkulacky
 * a jej autorov.
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#include "help.h"
#include "ui_help.h"


/*!
 * \brief Konstruktor okna.
 */
Help::Help(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Help)
{
    ui->setupUi(this);
}
/*!
 * \brief Destruktor okna.
 */
Help::~Help()
{
    delete ui;
}
/*!
 * \brief Osetrenie stlacenia klavesy ESC rusi okno.
 * \param Udalost stlacenia klavesy.
 */
void Help::keyPressEvent(QKeyEvent *event) {
    switch(event->key()){
    case Qt::Key_Escape:
        this->close();
    }
}
