/*!
 * \file matlib.h.
 * \brief Kniznica implementujuca matematicke funkcie vyuzivajuce pri kalkulacke
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */

#ifndef MATLIB_H
#define MATLIB_H

/*!
* \brief Zaokruhlovanie smerom dole.
* \param Cislo ktore sa ma zaokruhlit.
* \return Zaokruhlena hodnota zadaneho cisla smerom dole.
*/
double rounddown(double number);
/*!
* \brief Scitanie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Scitana hodnota dvoch operandov.
*/
double plus(double lastNumber, double currentNumber);
/*!
* \brief Odcitanie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Odcitana hodnota dvoch operandov.
*/
double minus(double lastNumber, double currentNumber);
/*!
* \brief Delenie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Delena hodnota dvoch operandov.
*/
double divide(double lastNumber, double currentNumber);
/*!
* \brief Nasobenie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Znasobena hodnota dvoch operandov.
*/
double multiply(double lastNumber, double currentNumber);
/*!
* \brief Umocnenie dvoch operandov.
* \param Mocnenec
* \param Mocnitel
* \return Umocnena hodnota dvoch operandov.
*/
double power(double lastNumber, double currentNumber);
/*!
* \brief Modulo operacia
* \param Delenec
* \param Delitel
* \return Zvysok po deleni.
*/
double modulo(double lastNumber, double currentNumber);
/*!
* \brief Faktorial zadaneho cisla.
* \param Operand.
* \return Faktorial zadaneho cisla.
*/
double factorial(double lastNumber);
/*!
* \brief Odmocnina dvoch.
* \param Operand.
* \return Odmocnina zadaneho cisla.
*/
double sqroot(double lastNumber);
/*!
* \brief Obrateny zlomok
* \param Operand
* \return Hodnota obrateneho zlomku.
*/
double frac(double lastNumber);

#endif // MATLIB_H
