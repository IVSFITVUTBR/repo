/*!
 * \file matlib.cpp.
 * \brief Kniznica implementujuca matematicke funkcie vyuzivajuce pri kalkulacke
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */

#include <cmath>
#include <math.h>
/*!
* \brief Zaokruhlovanie smerom dole.
* \param Cislo ktore sa ma zaokruhlit.
* \return Zaokruhlena hodnota zadaneho cisla smerom dole.
*/
double rounddown(double number){
    return (double)((long long) number);
}
/*!
* \brief Scitanie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Scitana hodnota dvoch operandov.
*/
double plus(double lastNumber, double currentNumber){
    return lastNumber + currentNumber;
}
/*!
* \brief Odcitanie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Odcitana hodnota dvoch operandov.
*/
double minus(double lastNumber, double currentNumber){
    return lastNumber - currentNumber;
}
/*!
* \brief Nasobenie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Znasobena hodnota dvoch operandov.
*/
double multiply(double lastNumber, double currentNumber){
    return lastNumber * currentNumber;
}
/*!
* \brief Delenie dvoch operandov.
* \param Operand cislo jedna.
* \param Operand cislo dva.
* \return Delena hodnota dvoch operandov.
*/
double divide(double lastNumber, double currentNumber){
    if(currentNumber == 0){
        return INFINITY;
    }
    return lastNumber / currentNumber;
}
/*!
* \brief Umocnenie dvoch operandov.
* \param Mocnenec
* \param Mocnitel
* \return Umocnena hodnota dvoch operandov.
*/
double power(double lastNumber, double currentNumber)
{
    double result = 1;
    for(int i = 0; i < currentNumber; i++){
        result *= lastNumber;
    }
    return result;
}
/*!
* \brief Faktorial zadaneho cisla.
* \param Operand.
* \return Faktorial zadaneho cisla.
*/
double factorial(double lastNumber)
{
    if(lastNumber < 0){
        return NAN;
    }
    double result = 1;
    for (int i = lastNumber; i>0; i--){
        result *= i;
    }
    return result;
}
/*!
* \brief Modulo operacia
* \param Delenec
* \param Delitel
* \return Zvysok po deleni.
*/
double modulo(double lastNumber, double currentNumber){
    if(currentNumber == 0){
        return NAN;
    }
    double rest = 0;
    while(lastNumber >= 0){
        rest = lastNumber;
        lastNumber -= currentNumber;
    }
    return rest;
}

double log2(double lastNumber){

    int result;
    std::frexp(lastNumber, &result);
    return (double)(result-1);

}
/*!
* \brief Odmocnina dvoch.
* \param Operand.
* \return Odmocnina zadaneho cisla.
*/
double sqroot(double lastNumber)
{

    double i=0;
    double x1,x2;
    while( (i*i) <= lastNumber)
           i+=0.1f;
    x1=i;
    for(int j=0;j<10;j++){
       x2=lastNumber;
       x2/=x1;
       x2+=x1;
       x2/=2;
       x1=x2;
    }
    return x2;
}
/*!
* \brief Obrateny zlomok
* \param Operand
* \return Hodnota obrateneho zlomku.
*/
double frac(double lastNumber)
{
    if(lastNumber == 0){
        return NAN;
    }
    return 1/lastNumber;
}
