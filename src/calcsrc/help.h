/*!
 * \class Help
 * \file help.h.
 * \brief Trieda deklarujuca spravanie HELP okna.
 * \author Daniel Florek, Martin Grnac.
 * \date 19 Apr 2018.
 */


#ifndef HELP_H
#define HELP_H

#include <QWidget>
#include <QKeyEvent>

namespace Ui {
class Help;
}

class Help : public QWidget
{
    Q_OBJECT

public:
    /*!
    * \brief Konstruktor okna.
    */
    explicit Help(QWidget *parent = 0);
    /*!
    * \brief Destruktor okna.
    */
    ~Help();

private slots:
    /*!
    * \brief Osetrenie stlacenia klavesy ESC rusi okno.
    */
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::Help *ui;
};

#endif // HELP_H
