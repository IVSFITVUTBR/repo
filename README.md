README pre IVS projekt 2

Náplň projektu:

1. Vytvorenie, návrh a implementácia jednoduchej kalkulačky.
2. Testy a profiling matematickej knižnice.
3. Vytvorenie mockupu vylepšenej verzie kalkulačky
4. Dokumentácia programu
5. Užívateľská dokumentácia

Riešitelia:

1. Daniel Florek (xflore02)
2. Martin Grnáč (xgrnac00)
3. Andrej Hučko (xhucko01)
