TERMINY
-------
Jednotlive datumy predstavuju milniky, ktorych body by mali byt do daneho datumu vykonane
-----------------------------------------------------------------------------------------
25.3 - Vytvorenie konceptu kalkulacky, navrhnutie gui, navrhnutie funkcionality, zvolenie vhodnych algoritmov 
	   na implementaciu matematickych funkci a riadiaceho automatu/enginu kalkulacky, navrh testovania

1.3 - Implementacia matematickych funkcii, implementacia riadiaceho automatu/enginu kalkulacky. Implementacia gui kalkulacky

8.4 - Implementacia testovania, vratane otestovania funkcionality kalkulacky + debugging. Odsledovanie vysledkov testovania, oprava buggov

15.4 - Vytvorenie dokumentacie + mockup dalsej verzie kalkulacky

24.4 - Odovzdanie

ULOHY
-----
Daniel Florek (xflore02) - Implementacia funkcionality kalkulacky, implementacia gui 
Martin Grnáč (xgrnac00) - Testy, Profiling, Navrh mockupu verzie 2
Andrej Hucko (xhucko01) - Uprava designu gui, dokumentacia

KOMUNIKACIA
-----------
Facebook, Osobne stretnutia popripade Skype konferencia

SYSTEM PRE SPRAVU VERZII
------------------------
Repozitar: Bitbucket
Link: https://bitbucket.org/IVSFITVUTBR/repo - skrz webove rozhranie

